<?php
/*-----------------------------------------------------------------------------------*/
/* This template will be called by all other template files to finish
/* rendering the page and display the footer area/content
/*-----------------------------------------------------------------------------------*/
?>

<!-- Modal -->
<div class="modal fade" id="donateInfo" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content rounded-0 bg-dark-blue ">



            <div class="modal-body text-white text-center">

                <a style="cursor: pointer;"  class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </a>

                <p class="pt-5 px-4 mx-auto">
                    Our online donation function will be available soon.
                    In the meantime, you may donate through our bank account below. Any amount you donate will go a long way to support our cause.
                </p>

                <div class="h5 px-4 font-weight-normal">Account No.: <strong>1226-70000972</strong>    </div>
                <div class="h5 px-2 font-weight-normal">Account Name:  <strong>Volunteers Against Poverty Foundation Inc.</strong>    </div>
                <div class="h5 px-4 font-weight-normal">Bank Name: <strong>PHILIPPINE NATIONAL BANK</strong>    </div>


                <p class="px-5 py-3 mx-auto">
                    If you wish to confirm your donation and be acknowledged on the website, please email to us your name and your deposit confirmation slip to admin@volunteersagainstpoverty.org.ph,
                    We thank you for your generosity.

                </p>
            </div>

        </div>
    </div>
</div>

<footer>
<!--    <div class="h-50 bg-white">-->
<!--        <div class="container h-100 pt-3 pb-3">-->
<!--            <div class="row  h-100 align-items-center">-->
<!--                <div class="col-lg-6 col-12 order-lg-1 order-2 text-lg-left  text-center ">-->
<!--                    <ul class="m-0 footer-list pl-0">-->
<!--                        <li>COOKIES</li>-->
<!--                        <li>PRIVACY</li>-->
<!--                        <li>TERM & CONDITION</li>-->
<!--                    </ul>-->
<!--                </div>-->
<!--                <div class="col-lg-6 col-12 order-lg-2 order-1 text-lg-right  text-center">FOLLOW US ON FACEBOOK</div>-->
<!--            </div>-->
<!--        </div>-->
<!--    </div>-->
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-12 text-lg-left  text-center copyright">
                    &copy; 2019 VAPF PH | ALL RIGHTS RESERVED
                </div>
                <div class="col-lg-6 text-right d-lg-block d-sm-none d-none smooth">
<!--                    <a href="#who"><img src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/up.svg"></a>-->
                    <script type="text/javascript"> //<![CDATA[
                        var tlJsHost = ((window.location.protocol == "https:") ? "https://secure.trust-provider.com/" : "http://www.trustlogo.com/");
                        document.write(unescape("%3Cscript src='" + tlJsHost + "trustlogo/javascript/trustlogo.js' type='text/javascript'%3E%3C/script%3E"));
                        //]]>
                    </script>
                    <script language="JavaScript" type="text/javascript">
                        TrustLogo("https://www.positivessl.com/images/seals/positivessl_trust_seal_sm_124x32.png", "POSDV", "none");
                    </script>
                </div>
            </div>
        </div>
       <!-- <div class="filler"></div>-->
</footer>

</body>
</html>
