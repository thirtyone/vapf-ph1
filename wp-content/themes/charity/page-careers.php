<?php
/*-----------------------------------------------------------------------------------*/
/* This template will be called by all other template files to begin
/* rendering the page and display the header/nav
/*-----------------------------------------------------------------------------------*/
?>
<!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
    <meta charset="<?php bloginfo('charset'); ?>" />
    <!--    <meta name="viewport" content="width=device-width, user-scalable=no"/>-->
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="format-detection" content="telephone=no">
    <title>
        <?php bloginfo('name'); // show the blog name, from settings ?> |
        <?php is_front_page() ? bloginfo('description') : wp_title(''); // if we're on the home page, show the description, from the site's settings - otherwise, show the title of the post or page ?>
    </title>
    <!--    <link rel="stylesheet" href="http://getbootstrap.com.vn/examples/equal-height-columns/equal-height-columns.css" />-->
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php // We are loading our theme directory style.css by queuing scripts in our functions.php file,
	// so if you want to load other stylesheets,
	// I would load them with an @import call in your style.css
	?>

    <?php // Loads HTML5 JavaScript file to add support for HTML5 elements in older IE versions. ?>
    <!--[if lt IE 9]>
    <script src="<?php echo get_template_directory_uri(); ?>/js/html5.js" type="text/javascript"></script>
    <![endif]-->

   <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,700|Playfair+Display:400,700,900&display=swap" rel="stylesheet">

    <?php wp_head();
	// This fxn allows plugins, and Wordpress itself, to insert themselves/scripts/css/files
	// (right here) into the head of your website.
	// Removing this fxn call will disable all kinds of plugins and Wordpress default insertions.
	// Move it if you like, but I would keep it around.
	?>
    <link href="<?php echo get_template_directory_uri(); ?>/assets/images/favicon.ico?v=2" />
    
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-56469068-17"></script>
    <script>
        window.dataLayer = window.dataLayer || [];

        function gtag() {
            dataLayer.push(arguments);
        }
        gtag('js', new Date());

        gtag('config', 'UA-56469068-17');

    </script>
</head>

<body class="" id="body">
    <header>
        <nav class="navbar fixed-top navbar-expand-lg navbar-light bg-white custom-navbar">
            <div class="container">
                <a class="navbar-brand" href="<?php bloginfo('url');?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.svg"></a>
                <button class="custom-toggler navbar-toggler navbar-toggler-right" type="button">
                    <span> </span>
                    <span> </span>
                    <span> </span>
                </button>

                <div class="collapse navbar-collapse" id="nav">
                    <ul class="navbar-nav ml-auto smooth">
                        <li class="nav-item">
                            <a class="nav-link pl-4" href="<?php bloginfo('url');?>">Who We Are</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  pl-4" href="<?php bloginfo('url');?>">Our Vision</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  pl-4" href="<?php bloginfo('url');?>">Our Mission</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  pl-4" href="<?php bloginfo('url');?>">Our Team</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  pl-4" href="<?php bloginfo('url');?>">Contact Us</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link  pl-4 active" href="<?php bloginfo('url');?>/careers/">Careers</a>
                        </li>
                        

                    </ul>

                </div>

            </div>

        </nav>
        <div class="sub-nav navbar position-fixed  bg-dark-blue w-100 text-white">
            <div class="container text-center">

                <span>Account No.: <strong class="mr-3">1226-70000972</strong></span>
                <span>Account Name: <strong class="mr-3">Volunteers Against Poverty Foundation Inc.</strong></span>
                <span>Bank Name: <strong class="mr-3">PHILIPPINE NATIONAL BANK</strong></span>
                <a class="btn btn-white  px-4 btn-radius-20 font-weight-bold d-none d-xl-block " data-toggle="modal" data-target="#donateInfo">Donate Today</a>

            </div>
        </div>


        <nav id="mainMenu" class="main-sidebar">
            <div class="h-remaining sidebar-wrapper text-white">
                <div class="row pt-5 h-70  text-right">
                    <div class="col-12">
                        <ul class="sidebar-list p-0 m-0 sidebar">
                            <li class="sidebar-item"><a href="<?php bloginfo('url');?>" class="sidebar-link h5">Who We Are</a></li>
                            <li class="sidebar-item"><a href="<?php bloginfo('url');?>" class="sidebar-link h5">Our Vision</a></li>
                            <li class="sidebar-item"><a href="<?php bloginfo('url');?>" class="sidebar-link h5">Our Mission</a></li>
                            <li class="sidebar-item"><a href="<?php bloginfo('url');?>" class="sidebar-link h5">Our Team</a></li>
                            <li class="sidebar-item"><a href="<?php bloginfo('url');?>" class="sidebar-link h5">Contact Us</a></li>
                            <li class="sidebar-item"><a href="<?php bloginfo('url');?>/careers/" class="sidebar-link h5">Careers</a></li>
                        </ul>
                    </div>

                </div>

                <!--<div class="row h-25  text-right">
                    <div class="col-12">
                        <ul class="sidebar-list p-0 m-0 sidebar">
                            <li class="sidebar-item"><small class="sidebar-link ">FOLLOW US ON FACEBOOK</small></li>
                            <li class="sidebar-item"><small class="sidebar-link ">PRIVACY</small></li>
                            <li class="sidebar-item"><small class="sidebar-link ">TERMS AND CONDITION</small></li>

                        </ul>
                    </div>

                </div>-->

            </div>
        </nav>


    </header>
    <div id="overlay" class="overlay"></div>



<?php if ( have_posts() ) : 
			// Do we have any posts/pages in the databse that match our query?
			?>

<?php while ( have_posts() ) : the_post(); 
				// If we have a page to show, start a loop that will display it
				?>


<section class="bg-white primary-section text-white">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-12">
                <div class="career-block">
                     <h1 class="mb-4">Available Careers</h1>
                     
        <?php
        // the query
        /*$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;     */
        $args = [
            'pagename'  => 'careers'
        ];
        $the_query = new WP_Query($args); ?>
        <?php $num = 0; ?>
        <?php if ($the_query->have_posts()) : ?>
        <!-- pagination here -->
        <!-- the loop -->
        <?php while ($the_query->have_posts()) : $the_query->the_post(); ?>

        <?php $c = get_the_category(); ?>



            <?php
                    $i = 0;
                    $rows = get_field('job_details');
                    if($rows)
                    {
                        foreach($rows as $row)
                        { ?>
				
        
                     <div class="block mb-3">
                         <h3 class="text-uppercase"><?php echo $row['position']; ?></h3>
                         <span><?php echo $row['description']; ?></span>
                         <p><?php echo $row['requirements']; ?></p>
                     </div>     		


            <?php
                            $i++;
                        }
                    } ?>

        <?php $num ++; ?>
        <?php endwhile; ?>
        <!-- end of the loop -->
        <!-- pagination here -->
        <?php wp_reset_postdata(); ?>
        <?php else : ?>

      <div>
        <h5 class="my-5 text-center">No Jobs Uploaded!</h5>
      </div>


<?php endif; ?>                        
                     
 
                                                     
                </div>
            </div>
            <div class="col-lg-6 col-12">
                <div class="career-block bg-dark-blue p-4">
                    <h2> Interested?</h2>

                    <p>Send a little something about yourself to info@volunteersagainstpoverty.org.ph with links to your work or fill out the form below.</p>
                    
                    <?php echo do_shortcode('[contact-form-7 id="15" title="Careers"]'); ?>
                </div>
            </div>
        </div>
    </div>

</section>
    <style>
        .primary-section {
            padding: 120px 0 100px 0;
        }
        .block {
            border-bottom: 1px dotted #999;
            background: rgba(0,0,0,0.3);
            padding: 15px;
        }
        .block span {
            font-size: 1rem;
            line-height: normal;
        }
        .block p {
            font-size: 0.825rem;
        }
        .sub-nav {
            margin-bottom: 50px;
        }
        .wpcf7-select {
            text-indent: 3px;
        }
        html {
            margin: 0!important;
        }
    </style>

<?php endwhile; // OK, let's stop the page loop once we've displayed it ?>

<?php else : // Well, if there are no posts to display and loop through, let's apologize to the reader (also your 404 error) ?>

<article class="post error">
    <h1 class="404">Nothing posted yet</h1>
</article>

<?php endif; // OK, I think that takes care of both scenarios (having a page or not having a page to show) ?>

<?php get_footer(); // This fxn gets the footer.php file and renders it ?>
